package com.example.exercisebroadcast.broadcast

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast

class MyBroadCastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val sb = StringBuilder()
        sb.append(
            """
                    Action: ${intent!!.action.toString()}
                    
                    """.trimIndent()
        )
        sb.append(
            """
                    URI: ${intent!!.toUri(Intent.URI_INTENT_SCHEME)}
                    
                    """.trimIndent()
        )
        val log = sb.toString()
        Log.d("TAG", log)
        Toast.makeText(context, log, Toast.LENGTH_LONG).show()
    }
}
