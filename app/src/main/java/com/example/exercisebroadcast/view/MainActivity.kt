package com.example.exercisebroadcast.view

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.exercisebroadcast.R
import com.example.exercisebroadcast.broadcast.BroadCastReceiver
import com.example.exercisebroadcast.broadcast.CustomBroadCastReceiver
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var br: BroadcastReceiver
    lateinit var cus: CustomBroadCastReceiver
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        br = BroadCastReceiver()
        val filter = IntentFilter().apply {
            addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED)
        }
        registerReceiver(br, filter)
        cus = CustomBroadCastReceiver()
        val filter1 = IntentFilter().apply {
            addAction("com.example.broadcast.BroadCast1")
        }
        registerReceiver(cus, filter1)
    }

    override fun onResume() {
        super.onResume()
        buttonSendBroadCast.setOnClickListener {
            Intent().also { intent ->
                intent.setAction("com.example.broadcast.BroadCast1")
                intent.putExtra("data", "Nothing to see here, move along.")
                sendBroadcast(intent)
            }
        }
        buttonStartActivity.setOnClickListener {
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent)
        }
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(br)
        unregisterReceiver(cus)
    }
}
