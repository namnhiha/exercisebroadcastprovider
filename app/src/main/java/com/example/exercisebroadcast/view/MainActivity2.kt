package com.example.exercisebroadcast.view

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.exercisebroadcast.R
import com.example.exercisebroadcast.provider.UserProvider
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        return true
    }

    @SuppressLint("Range")
    override fun onResume() {
        super.onResume()
        btnAdd.setOnClickListener {
            val values = ContentValues()
            values.put(UserProvider.name, txtName.text.toString())
            contentResolver.insert(UserProvider.CONTENT_URI, values)
            Toast.makeText(baseContext, "New Record Insert", Toast.LENGTH_LONG).show()
        }
        btnRetrieve.setOnClickListener {
            var cursor =
                contentResolver.query(
                    Uri.parse("content://com.example.exercisebroadcast.provider.UserProvider/users"),
                    null,
                    null,
                    null,
                    null
                )
            if (cursor!!.moveToFirst()) {
                val strbuild = StringBuilder()
                while (!cursor.isAfterLast) {
                    strbuild.append(
                        "\n" + cursor.getString(cursor.getColumnIndex("id")) + "-" + cursor.getString(
                            cursor.getColumnIndex("name")
                        )
                    )
                    cursor.moveToNext()
                }
                result.text = strbuild
            } else {
                result.text = "No Records Found"
            }
        }
    }
}
