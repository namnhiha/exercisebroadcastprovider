package com.example.exercisebroadcast.provider

import android.content.*
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteQueryBuilder
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi

class UserProvider : ContentProvider() {
    private var db: SQLiteDatabase? = null
    private val values: HashMap<String, String>? = null

    init {
        uriMatcher = UriMatcher(UriMatcher.NO_MATCH)
        uriMatcher.addURI(PROVIDER_NAME, "users", uriCode)
        uriMatcher.addURI(PROVIDER_NAME, "users/*", uriCode)
    }

    override fun getType(uri: Uri): String? {
        when (uriMatcher.match(uri)) {
            uriCode -> {
                return "vnd.android.cursor.dir/users"
            }
            else -> {
                throw IllegalArgumentException("Unsupported URI:$uri")
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onCreate(): Boolean {
        val dbHelper = context?.let { DatabaseHelper(it) }
        db = dbHelper?.writableDatabase!!
        return db != null
    }

    override fun query(
        uri: Uri,
        projection: Array<out String>?,
        selection: String?,
        selectionArgs: Array<out String>?,
        sortOrder: String?
    ): Cursor? {
        val qb = SQLiteQueryBuilder()
        qb.tables = TABLE_NAME
        when (uriMatcher.match(uri)) {
            uriCode -> {
                qb.projectionMap = values
            }
            else -> java.lang.IllegalArgumentException("Unknown URI" + uri)
        }
        if (sortOrder == null || sortOrder == "") {

        }
        var cursor = qb.query(db, projection, selection, selectionArgs, null, null, id)
        cursor.setNotificationUri(context?.contentResolver, uri)
        return cursor
    }

    override fun insert(uri: Uri, values: ContentValues?): Uri? {
        val rowId = db?.insert(TABLE_NAME, "", values)
        rowId?.let {
            if (rowId > 0) {
                val _uri = ContentUris.withAppendedId(CONTENT_URI, rowId)
                context?.contentResolver?.notifyChange(_uri, null)
                return _uri
            }
        }
        throw SQLiteException("Failed to add record into" + uri)
    }

    override fun update(
        uri: Uri,
        values: ContentValues?,
        selection: String?,
        selectionArgs: Array<out String>?
    ): Int {
        var count = 0
        when (uriMatcher.match(uri)) {
            uriCode -> {
                count = db?.update(TABLE_NAME, values, selection, selectionArgs)!!
                throw java.lang.IllegalArgumentException("Unknown URI" + uri)
            }
        }
        context?.contentResolver?.notifyChange(uri, null)
        return count
    }

    override fun delete(uri: Uri, selection: String?, selectionArgs: Array<out String>?): Int {
        var count = 0
        when (uriMatcher.match(uri)) {
            uriCode -> {
                count = db?.delete(TABLE_NAME, selection, selectionArgs)!!
            }
            else -> java.lang.IllegalArgumentException("Unknown Uri" + uri)
        }
        context?.contentResolver?.notifyChange(uri, null)
        return count
    }

    companion object {
        const val PROVIDER_NAME = "com.example.exercisebroadcast.provider.UserProvider"
        val URL = "content://" + PROVIDER_NAME + "/users"
        val CONTENT_URI = Uri.parse(URL)
        val id = "id"
        val name = "name"
        val uriCode = 1
        lateinit var uriMatcher: UriMatcher
//        private var value: HashMap<String, String>

        //SQL Lite
        const val DATABASE_NAME = "EmpDB"
        const val DATABASE_VERSION = 1
        const val TABLE_NAME = "Employees"
        val CREATE_DB_TABLE =
            " CREATE TABLE $TABLE_NAME (id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT NOT NULL);"

        class DatabaseHelper(context: Context) :
            SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
            override fun onCreate(db: SQLiteDatabase?) {
                db?.execSQL(CREATE_DB_TABLE)
            }

            override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
                db?.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
                onCreate(db)
            }
        }
    }
}
